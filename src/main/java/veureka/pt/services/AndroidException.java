package veureka.pt.services;

import android.content.Context;

public class AndroidException extends AException {
    
    public static AndroidException make(Context context, String errorId, Throwable aThrowable, Object... arguments) {
        if(aThrowable == null) {
            aThrowable = new RuntimeException();
        }
        if(aThrowable instanceof AndroidException) {
            return (AndroidException) aThrowable;
        } else {
            String messageError = BundleManager.getInstance().displayMessage(context, errorId, arguments);
            return new AndroidException(errorId, messageError, aThrowable);
        }
    }
    
    /**
     * Para manejo de excepciones
     *
     * @param idError ID del error
     * @param aMessage Mensaje correspondiente al código de error
     * @param aThrowable La excepcion que se genero
     */
    private AndroidException(String idError, String aMessage, Throwable aThrowable) {
        super(idError, aMessage, aThrowable);
    }
}