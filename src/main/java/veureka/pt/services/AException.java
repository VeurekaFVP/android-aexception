package veureka.pt.services;

import java.io.PrintWriter;
import java.io.StringWriter;

public abstract class AException extends Exception {
    
    private static final long serialVersionUID = 1L;
    private static final String NEW_LINE = System.getProperty("line.separator");
    private final String errorId;
    private final String messageError;
    
    AException(String idError, String messageError, Throwable aThrowable) {
        super(aThrowable.getMessage(), aThrowable);
        this.errorId = idError;
        this.messageError = messageError;
    }
    
    @SuppressWarnings("unused")
    public String getStrStackTrace() {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        this.printStackTrace(pw);
        pw.flush();
        return sw.toString();
    }
    
    @Override
    public String getMessage() {
        String message = super.getMessage();
        return this.errorId + " - " + this.messageError + NEW_LINE + "Caused by: " + message + NEW_LINE;
    }
}
